/**
* Fucntions that are used by CreateTable() of
* GenericHashTable, to initialize hash table
* which's elements are strings. 
*
*/

#include <stdio.h> 
#include <string.h>
#include "MyStringFunctions.h"


/**
* Hash function for hash table which's elements are strings.
* Returns location in hash table, by the formula: 
* (ascii sum of string characters) modulo tableSize.
*
*/
int StringFcn(const void *key, size_t tableSize)
{
	char* str = (char*)key;
	int i = 0;
	int asciiSum = 0;
	int tableSiz = (int)tableSize; /* to prevent conversion problems on modulo operation. */

	while (str[i] != '\0')
	{
		asciiSum += (int)str[i];
		i++;
	}

	return (asciiSum % tableSiz);
}

/**
* Prints the string key (of hash table of strings).
*
*/
void StringPrint(const void *key)
{
	char* str = (char*)key;

	printf("%s", str);
}


/**
* Compares two hash table keys, which are strings.
* Returns 0 if keys are equal, and non-zero otherwise.
*
*/
int StringCompare(const void *key1, const void *key2)
{
	char* str1 = (char*)key1;
	char* str2 = (char*)key2;

	return (strcmp(str1, str2));
}