FLAGS= -Wall -Wvla -lm -g

GenericHashTable: GenericHashTable.o
	ar rcs libGenericHashTable.a GenericHashTable.o
	ranlib libGenericHashTable.a

all: GenericHashTable HashIntSearch HashStrSearch

HashIntSearch: HashIntSearch.o MyIntFunctions.o TableErrorHandle.o libGenericHashTable.a
	gcc $(FLAGS) HashIntSearch.o MyIntFunctions.o TableErrorHandle.o libGenericHashTable.a -o HashIntSearch

HashStrSearch: HashStrSearch.o MyStringFunctions.o TableErrorHandle.o libGenericHashTable.a
	gcc $(FLAGS) HashStrSearch.o MyStringFunctions.o TableErrorHandle.o libGenericHashTable.a -o HashStrSearch

HashIntSearch.o: HashIntSearch.c
	gcc -c $(FLAGS) HashIntSearch.c	

HashStrSearch.o: HashStrSearch.c
	gcc -c $(FLAGS) HashStrSearch.c

MyIntFunctions.o: MyIntFunctions.c MyIntFunctions.h
	gcc -c $(FLAGS) MyIntFunctions.c 

MyStringFunctions.o: MyStringFunctions.c MyStringFunctions.h
	gcc -c $(FLAGS) MyStringFunctions.c

GenericHashTable.o: GenericHashTable.c GenericHashTable.h TableErrorHandle.h
	gcc -c $(FLAGS) GenericHashTable.c

TableErrorHandle.o: TableErrorHandle.c TableErrorHandle.h
	gcc -c $(FLAGS) TableErrorHandle.c

.PHONY: all clean

clean:
	rm -f TableErrorHandle.o GenericHashTable.o MyStringFunctions.o MyIntFunctions.o HashStrSearch.o HashIntSearch.o libGenericHashTable.a HashStrSearch HashIntSearch

