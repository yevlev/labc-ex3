/**
* An implementation of the Generic Hash Table interface.
*
*/

#include "GenericHashTable.h"

/**
* power function for integers.
* returns base^exp.
*/
static int myPow(int base, int exp)
{
	int i;
	int res = 1;

	for (i = 0; i < exp; i++)
	{
		res = res * base;
	}

	return res;
}

/**
 * Allocate memory for an object which points to the given key.
 * If run out of memory, free all the memory that was already allocated by the function, 
 * report error MEM_OUT to the standard error and return NULL.
 */
ObjectP CreateObject(void *key) 
{
	ObjectP ret;

	ret = (ObjectP)malloc(sizeof(Object));
	
	if (ret == NULL)   /* memory allocation failed. */ 
	{
		ReportError(MEM_OUT);
		return NULL;
	}

	ret->key = key; 
	ret->next = NULL;

	return ret;
}


/**
 * Allocate memory for a hash table with which uses the given functions.
 * tableSize is the number of cells in the hash table.
 * If run out of memory, free all the memory that was already allocated by the function, 
 * report error MEM_OUT to the standard error and return NULL.
 */
TableP CreateTable(size_t tableSize, HashFcn hfun, PrintFcn pfun, ComparisonFcn fcomp) 
{
	int i;
	TableP ret;

	ret = (TableP)malloc(sizeof(Table)); 

	if (ret == NULL)   /* memory allocation of struct Table failed. */
	{
		ReportError(MEM_OUT);
		return NULL;
	}
	
	ret->tableArray = (ObjectP*)malloc(sizeof(ObjectP) * tableSize); 

	if (ret->tableArray == NULL)   /* memory allocation of tableArray failed. */
	{
		ReportError(MEM_OUT);
		free(ret);
		return NULL;
	}
	
	for (i = 0; i < tableSize; i++)  /* initializing all entries (heads of LL) to NULL. */
	{
		ret->tableArray[i] = NULL;
	}

	/* init. other struct Table fields. */
	ret->initSize = tableSize;
	ret->numExpansions = 0;
	ret->currTableSize = tableSize;

	ret->hashFun = hfun;
	ret->printFun = pfun;
	ret->compFun = fcomp;

	return ret;
}

/**
 * returns TRUE iff linked list, which's head is given,
 * is "full", i.e. has MAX_ELEMENTS_IN_ENTRY links.
 */
static Boolean isEntryFull(ObjectP head)
{
	int counter = 0;

	while (head != NULL)
	{
		head = head->next; 
		counter++;
	}
	
	if (counter == MAX_ELEMENTS_IN_ENTRY)
	{
		return TRUE;
	}
	
	return FALSE;
}


/**
 * doubles the current table size, and copies old values to duplicated table.
 * returns FALSE if an error occured (memory alloc.), and TRUE otherwise.
 */
static Boolean duplicateTable(TableP table)
{
	ObjectP* oldTableArray = table->tableArray;  /* keeping the old table array. */

	int oldArraySize = table->currTableSize;
	int newArraySize = oldArraySize * EXPANSION_RATE;
	int i;

	table->tableArray = (ObjectP*)malloc(sizeof(ObjectP) * newArraySize);

	if (table == NULL) /* allocation of new table array failed. */
	{
		table->tableArray = oldTableArray;  /* putting back the old array. */
		return FALSE;
	}

	table->numExpansions++;
	table->currTableSize = newArraySize;

	for (i = 0; i < oldArraySize; i++) 
	{
		/* copying the old values to the new array (at correct indexes). */
		table->tableArray[i * EXPANSION_RATE] = oldTableArray[i];
	}

	free(oldTableArray);  /* freeing memory of old table array (just the array itself). */

	for (i = 1;  i < newArraySize; i = i + 2) /* initializing other (non-filled) cells to NULL. */
	{
		table->tableArray[i] = NULL;
	}

	return TRUE;
}

/**
* trying to insert given object to given table.
* on success (no need in duplication) - returns TRUE, on failure - FALSE. 
*/
static Boolean insertIfPossible(TableP table, ObjectP object)
{
	int hashInsertPosition, nextOriginalIndex;
	int tableExpanPower, i;
	ObjectP obj;

	tableExpanPower = myPow(EXPANSION_RATE, table->numExpansions); /* the d. */

	/* calculating by given formula. */
	hashInsertPosition = table->hashFun(object->key, table->initSize) * tableExpanPower;
	
	nextOriginalIndex = hashInsertPosition + tableExpanPower; /* for sure <= table size. */

	for (i = hashInsertPosition; i < nextOriginalIndex; i++) /* range of possible insertion. */
	{
		obj = table->tableArray[i];

		if (!isEntryFull(obj))  /* we can insert. */
		{
			if (obj == NULL)   /* entry is empty. */
			{
				table->tableArray[i] = object; 
			}
			else   /* entry has single element. */
			{
				table->tableArray[i]->next = object;   
			}

			return TRUE;
		}
	}

	return FALSE;
}


/**
 * Insert an object to the table.
 * If all the cells appropriate for this object are full, duplicate the table.
 * If run out of memory during the table duplication, report
 * MEM_OUT and do nothing (the table should stay at the same situation
 * as it was before the duplication).
 * If everything is OK, return TRUE. Otherwise (an error occured) return FALSE.
 */
Boolean InsertObject(TableP table, ObjectP object) 
{
	Boolean res;

	res = insertIfPossible(table, object); /* trying to insert. */
	
	if (res == FALSE) /* means that we need to duplicate the table. */
	{
		if (duplicateTable(table) == FALSE)  /* doubling the hash table failed (memory alloc.). */
		{
			ReportError(MEM_OUT);
			return FALSE;
		}

		 /* for sure returns true, as after single duplication, there is always room. */
		insertIfPossible(table, object);
	}

	return TRUE;
}


/**
 * Search the table and look for an object with the given key.
 * If such object is found fill its cell number into arrCell (where 0 is the
 * first cell), and its placement in the list into listNode (when 0 is the
 * first node in the list, i.e. the node that is pointed from the table
 * itself).
 * If the key was not found, fill both pointers with value of -1.
 */
void FindObject(ConstTableP table, const void* key, int* arrCell, int* listNode) 
{
	int i, linkCounter;
	int hashBaseInsertPosition, tableExpanPower, nextOriginalIndex;
	ObjectP link;

	tableExpanPower = myPow(EXPANSION_RATE, table->numExpansions); /* the d. */

	hashBaseInsertPosition = table->hashFun(key, table->initSize) * tableExpanPower;

	nextOriginalIndex = hashBaseInsertPosition + tableExpanPower;

	/* from insertion definition, searched key can only be in this range. */
	for (i = hashBaseInsertPosition; i < nextOriginalIndex; i++)
	{
		link = table->tableArray[i]; /* head. */
		linkCounter = 0;

		while (link != NULL)
		{
			if (table->compFun(link->key, key) == 0)  /* found. */
			{
				*arrCell = i;
				*listNode = linkCounter;
				return;
			}

			link = link->next;
			linkCounter++;
		}
	}

	*arrCell = -1;  /* not found. */
	*listNode = -1;
}


/**
 * Print the table (use the format presented in PrintTableExample).
 */
void PrintTable(ConstTableP table) 
{
	int i;
	ObjectP obj;

	for (i = 0; i < table->currTableSize; i++)
	{
		printf("[%d]\t", i);

		obj = table->tableArray[i];  /* head of i-th linked list. */

		while (obj != NULL)  /* printing linked-list by given format. */
		{
			table->printFun(obj->key);

			printf(OBJECT_DELIMINATOR);
			
			obj = obj->next;
		}

		printf("\n");
	}

}
	

/**
 * Free all the memory allocated for an object, including the memory allocated
 * for the key.
 * Use this function when you free the memory of the table.
 */
void FreeObject(ObjectP object)
{
	free(object->key);
	free(object);
}


/**
 * Free all the memory allocated for the table.
 * It's the user responsibility to call this function before exiting the program.
 */
void FreeTable(TableP table) 
{
	int i;
	ObjectP curr, temp;

	for (i = 0; i < table->currTableSize; i++)  /* frees all objects. */
	{
		curr = table->tableArray[i];
		
		while (curr != NULL)
		{
			temp = curr->next;
			FreeObject(curr);
			curr = temp;
		}
	}

	/* freeing the hash table array and table struct. */
	free(table->tableArray);
	free(table);
}



