/**
* Fucntions that are used by CreateTable() of
* GenericHashTable, to initialize hash table
* which's elements are integers. 
*
*/

#include <stdio.h>
#include "MyIntFunctions.h"


/**
* Hash function for hash table which's elements are integers.
* Returns location in hash table, by the formula: 
* (the integer) modulo tableSize (returns always positve value).
*
*/
int IntFcn(const void *key, size_t tableSize) 
{
	int k = *((int*)key);
	int tableSiz = (int)tableSize; /* casting to avoid problems with negative modulo. */

	int modRes = k % tableSiz;

	if (modRes < 0)  /* modRes was negative. */
	{
		modRes = modRes + tableSiz;
	}

	return modRes;  
}


/**
* Prints the integer key (of hash table of integers).
*
*/
void IntPrint(const void *key) 
{
	int k = *((int*)key);

	printf("%d", k);
}


/**
* Compares two hash table keys, which are integers.
* Returns 0 if keys are equal, and non-zero otherwise.
*
*/
int IntCompare(const void *key1, const void *key2)
{
	int k1 = *((int*)key1);
	int k2 = *((int*)key2);

	return (k1 - k2);
}
